// pages/register/login.js

import MD5 from '../../libs/js/md5.min';
import { getSms } from '../../utils/util'


const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        countryCodes: ["+86", "+80", "+84", "+87"],
        countryCodeIndex: 0,
        phone: '',
        msgCode: '',
        password: '',
        confirmPassword: '',
        lastTime: '60', //剩余时间
        isShow: true, //显示验证码倒计时
        btnDisabled: true
    },
    // 选择手机区号
    bindCountryCodeChange(e){
        this.setData({
            countryCodeIndex: e.detail.value
        })
    },
    // 正则校验手机号
    validatePhone(str){
        // 定义手机号的正则
        var isMobile = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        return isMobile.test(str);
    },
    // 输入手机号事件
    phoneInput(e) {
        var self = this;
        let value = e.detail.value;
        self.setData({phone: value});
        var isNotDisabled = self.validatePhone(value) && self.data.password.length > 0;
        if (isNotDisabled) {
            self.setData({
                btnDisabled: false
            })
        } else {
            self.setData({
                btnDisabled: true
            })
        }
    },
    //验证码
    codeInput: function (e) {
        this.setData({
            msgCode: e.detail.value
        })
    },
    //输入密码事件
    passwordInput(e) {
        var self = this;
        let value = e.detail.value;
        let phone = self.data.phone;
        self.setData({password: value});
        var isNotDisabled = self.validatePhone(phone) && value.length > 0;
        if (isNotDisabled) {
            self.setData({
                btnDisabled: false
            })
        } else {
            self.setData({
                btnDisabled: true
            })
        }
    },
    // 输入确认密码
    cPasswordInput(e) {
        var self = this;
        let value = e.detail.value;
        let phone = self.data.phone;
        self.setData({password: value});
        var isNotDisabled = self.validatePhone(phone) && value.length > 0;
        if (isNotDisabled) {
            self.setData({
                btnDisabled: false
            })
        } else {
            self.setData({
                btnDisabled: true
            })
        }
    },
    //发送手机验证码
    sendMsgCode() {
        let self = this;
        let params = {
            phone: self.data.phone
        };
        if(params.phone == ''){
            app.Toast.warn('请输入手机号码');
            return;
        }
        if (!self.validatePhone(params.phone)) {
            app.Toast.warn('请输入正确的手机号码');
            return;
        }
        app.Toast.loading();
        app.api
            .SendMsgCode(params)
            .then(res => {
                if (res.code == 0) {
                    getSms.setTime(self);
                    // 将获取验证码按钮隐藏60s，60s后再次显示
                    self.setData({
                        isShow: !self.data.isShow //false
                    })
                } else {
                    app.Toast.showModal(res.message);
                }
            })
            .catch(e => {
                app.Toast.showModal(e.message);
                console.error(e)
            })
    },
    // 注册
    register() {
        let self = this;
        let params = {
            phone: self.data.phone,
            msgCode: self.data.msgCode,
            password: MD5.base64(self.data.password)
        };
        if (!self.validatePhone(params.phone)) {
            app.Toast.warn('请输入正确的手机号码');
            return false;
        }
        app.Toast.loading();
        app.api
            .Register(params)
            .then(res => {
                if (res.code == 0) {
                    app.Toast.success('注册成功');
                    setTimeout(() => {
                        wx.navigateBack({
                            delta: 1
                        });
                    },1500);
                } else {
                    app.Toast.warn(res.message);
                    setTimeout(() => {
                        wx.navigateBack({
                            delta: 1
                        });
                    },1500);
                }
            })
            .catch(e => {
                app.Toast.error(e.message);
            })
    }
});