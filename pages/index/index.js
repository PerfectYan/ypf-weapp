
const app = getApp();

Page({
    data: {
        logo: '../../images/index/logo-bihang.png',
        userInfo: {
            avatarUrl: '../../images/index/icon-avatar.png'
        },
        bhSessionId: '',
        searchInput: '',
        activeCategoryId: '',
        categoryList: [
            { name: '推荐', id: 1},
            { name: '比特币', id: 2},
            { name: '哈哈', id: 3}
        ],
        allSiteList: [],
        siteList: [],
        loadingMoreHidden: true
    },
    onLoad() {
        let self = this;
        wx.setNavigationBarTitle({
            title: '币航'
        });
        wx.login({
            success: function (res) {
                if (res.code) {
                    //登录远程服务器
                    let params = {
                        code: res.code
                    };
                    app.api.Login(params).then(res => {
                        self.setData({
                            bhSessionId: res.data.bhSessionId || ''
                        });
                        wx.setStorageSync('bhSessionId', self.data.bhSessionId);
                        self.getSiteList();
                    });
                } else {
                    console.log(res);
                }
            },
            fail: function (err) {
                console.log(err);
            }
        });
    },
    onShow() {
        let userInfo = wx.getStorageSync('userInfo');
        let dialogComponent = this.selectComponent('.wxc-dialog');
        if (!userInfo) {
            dialogComponent && dialogComponent.show();
        } else {
            this.setData({
                userInfo: userInfo
            });
            dialogComponent && dialogComponent.hide();
        }
    },
    onConfirm(e) { // 点击允许
        let dialogComponent = this.selectComponent('.wxc-dialog');
        dialogComponent && dialogComponent.hide();
        let userInfo = JSON.parse(e.detail.detail.rawData);
        console.log('userInfo = ',userInfo);
        if (!userInfo) {
            return;
        }
        this.setData({
            userInfo: userInfo
        });
        wx.setStorageSync('userInfo', userInfo);
    },
    onCancel() { // 点击拒绝
        let dialogComponent = this.selectComponent('.wxc-dialog');
        dialogComponent && dialogComponent.hide();
    },
    getSiteList(categoryId) {
        let self = this;
        if(categoryId == undefined){
            categoryId = '';
        }
        let params = {
            categoryId: categoryId
        };
        app.api.GetSiteList(params).then(res => {
            if(categoryId == ''){
                let resArr = res.data;
                let categoryList = [];
                let siteList = [];
                resArr.map( item => {
                    let category = {};
                    category.categoryId = item.categoryId;
                    category.categoryName = item.categoryName;
                    category.categoryIndex = item.categoryIndex;
                    categoryList.push(category);
                    item.siteList.map(site => {
                        siteList.push(site);
                    });
                });
                self.setData({
                    activeCategoryId: categoryList[0].categoryId,
                    categoryList: categoryList,
                    siteList: resArr[0].siteList,
                    allSiteList: siteList
                });
                console.log('allSiteList = ',self.data.allSiteList);
            }else{
                self.setData({
                    siteList: res.data.siteList
                });
            }

        });
    },
    onSearchInput(e) {
        this.setData({
            searchInput: e.detail.value
        })
    },
    toSearch() {
        let self = this;
        if(self.data.searchInput == ''){
            return;
        }
        let allSiteList = self.data.allSiteList;
        let searchedSiteList = [];
        allSiteList.map(site => {
            // if(self.data.searchInput == site.siteName){
            //     searchedSiteList.push(site);
            // }
            if(site.siteName.indexOf(self.data.searchInput) > -1){
                searchedSiteList.push(site);
            }
        });
        this.setData({
            siteList: searchedSiteList
        });
    },
    tabClick(e) {
        this.setData({
            activeCategoryId: e.currentTarget.id
        });
        this.getSiteList(this.data.activeCategoryId);
    },
    showSiteUrl(e) {
        wx.showModal({
            title: '导航链接',
            content: e.currentTarget.dataset.url,
            showCancel: false
        })
    },
    onShareAppMessage: function () {
        return {
            title: '币航',
            path: '/pages/index/index',
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    },
});