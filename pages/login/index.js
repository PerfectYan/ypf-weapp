// pages/login/login.js
import MD5 from "../../libs/js/md5.min";

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        phone: '',
        password: '',
        btnDisabled: true
    },
    // 正则校验手机号
    validatePhone(str){
        var isMobile = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        return isMobile.test(str);
    },
    // 输入手机号事件
    phoneInput(e) {
        var self = this;
        let value = e.detail.value;
        self.setData({phone: value});
        var isNotDisabled = self.validatePhone(value) && self.data.password.length > 0;
        if (isNotDisabled) {
            self.setData({
                btnDisabled: false
            })
        } else {
            self.setData({
                btnDisabled: true
            })
        }
    },
    //输入密码事件
    passwordInput(e) {
        var self = this;
        let value = e.detail.value;
        let phone = self.data.phone;
        self.setData({password: value});
        var isNotDisabled = self.validatePhone(phone) && value.length > 0;
        if (isNotDisabled) {
            self.setData({
                btnDisabled: false
            })
        } else {
            self.setData({
                btnDisabled: true
            })
        }
    },
    // 登录
    login() {
        let self = this;
        // let params = {
        //     phone: self.data.phone,
        //     msgCode: self.data.msgCode,
        //     password: MD5.base64(self.data.password)
        // };
        let header = {
            mobileType: '3',
            appVersion: '0.1.1'
        };

        if (!self.validatePhone(self.data.phone)) {
            app.Toast.warn('请输入正确的手机号');
            return false;
        }

        var params = {
            certCode: '430524198810307472',
            code: '1234',
            mobileType: '3',
            password: MD5.base64(this.data.password),
            buildModel: ''
        };
        app.Toast.loading();
        app.api
            .Login(params,header)
            .then(res => {
                if (res.code == 0) {
                    wx.navigateBack({
                        delta: 1
                    });
                } else {
                    app.Toast.warn(res.message);
                }
            });
    },
    // 忘记密码
    resetPwd() {
        wx.redirectTo({
            url: '/pages/register/index',
        })
    },
    // 注册
    register() {
        wx.redirectTo({
            url: '/pages/register/index',
        })
    }
});