// components/dialog/dialog.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String
    },
    titleColor: {
      type: String,
      value: '#000000'
    },
    logImage: {
      type: String,
        value: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAA6CAYAAAB/AU2IAAAAAXNSR0IArs4c6QAAB5lJREFUeAHtXW2IFVUYvlublVtZukVFSulmqBUolRUktVKQIVSksWSF9SdCLAgTodI/RfajILH+RCkICraFFuQP/eFHH9SqqZVpalttRWbWVpqfuz3P7p27s7Pve865M+fenXvvvPA6M+/XOed95pw5c87sNZerIuru7m4ATwHPA7eCO5I2DzEeBmv0RNL4kj8Ka9MKhLxT8rHJ6m0GadKjkXWoz0XgRvBI8NXgsaHjGJyfCfZJzYZgGwy6VKnqkbxNqapRLkcwz8gzQeN5A3gEeDjYN5AIaaSpirajrq7uO0WXOjF79G2pq1UZK4Qb/RwUp41so6HjyCHRZvieJynysmO4EU4Z9GVV1aGy3WUtscyFIdkcIVRC89+D8j7VIL7iXhS9hu4oYw4OjxYRajxshyr2pyHfrugk8R7UY5Z2J0sOmSx+Bq6A6w3x3ft58tFVTKwejKsZaA6bu/ulqIYvqgXog8BwH/gb8LY878CQdQznGSEDBPqjEmWCM+TJDrE5R1gn2FHeFeJ/cX4I/Ef+SHAPgPcBUOoyMmSgHkmaZtDHVmECMgXOGx0CdJWqDg5l14xJtQzdSQDTZuVc9WpzCLwQNtMtdiehP2qxKZX6PwbOgMbbj5LhvRhptiq6ghgjFx8lRkKc52FAHjTiqlNGNZCBDGgDyOitQ8CnwV0RnmFwS6UqA9oAC4bcE1AfBvM5HubLDW6pVGVA22H5RTC5TJClWlSYjGFougU19dmA8Y4t53r7/Y62rman0BvXuhpb7Aj09RGbiuvRBaDRkAVg22tCpL1eLjmqtHqJ1BfkCE5NO0t9lvYzqUcnBho39xIU/aC9eGeLv3FzN2nWYaA1m1qXlwRoJPUC8MUekzvEFCt7Rpuy06uTgPb5iLPXwINFBrQ9ib8KJhdi6D1XkKdWlAFth0bq0fRK/Jy2F+3PIgPanstEQKPn+5oU2mtqsIgzGZuAeC7fQt0IuxWGsgMVP41xfRWjz1PgJ3niibRNjSA8gWZbomT7MHAuQH4FTivBi6LO5b6OAzQX+61Ao5GXujYG8fa62iKudRPBNZaLHerGPXFxFwt1Yf6GKnGaFbmL+G4Y/aAYsvNMUnSqOA7QarAKVWi7Vz3NAZjs8bxpR+f5GhzH5bkJx7PAvokfU/CLmQGE+vRsOw5QWAQ1CzQS9ghyczaYAEq0GDb8nvwqcEXNsKXG1CzQSMZyKSEhmfRcDqkr6zSbdVcWXrFrmwEdO3VOjvwqVZzIOXl7NKrlodtjGsVQj2NC9baoGQRh1qPjJ30/XFeDdykhyvoaqNShIM6ALqRCPCFY3wuaNvTWJvBM6LYK+tSJahnopUDjVbC2wnUXgGyEfm7qUItRoZp9RgPEOcwX3pX5x/TkKP0TFXi+Nv2dN1fjvFIt9+ggkVrCjwcGJTrywwONvH/snwHduzomJfyEJPQoG2aIdcSgi6XKgO792QwpebHWlKVAiswEdNajlaQlEWtD6O9Jgjr4auV2Yv7ArVuvlPXoXO4SIaPHkeySTcYwAeQkWPvujO/n3qkSgXbZFjRuPQZZRMK5ezUiuA4dS92br0NZ/JEciTKg81mRgIkmzPphRN6hCUfuN0epIyrwfG3aGRP3oZOWX1E9Gj3wfDR4ukOjXWfMk5VY3ypyX2LTj8185quQcJxULpgAUK5IjQxVlB8AjALPA7t8otQOOxfSfiyuZEDnHxf3KJXjQslmRZdInEqg0aIZ4DcStMy6/oyEc6GkWSnjK0XuQzwbQbRPhXdiEvinj0KiMdIKdLSexV6vcnB4ADbS6MBe9XHIn7Pv3aFrnkobHRGTgZe4uZjv+QM1Bcn7hTPPJ9UI9CfoFZsc8vSsYrMd/n8FOpxvxHkxnyMHrtJxIYRXSgrIeIMtU3SJxRU1GXNoLV+LWhzsaKJ9MrvO0T8ws344iJ7MPw1eAIfnAifhuAE31Y+C3FU0DGWMiRpDditl1dSjv0B7WhImizlZzn9ciADCbqKDLUePlyx2L1r0Luo1qFMrDHnD8+euObvnpHZSpQPNIZbP07fAHwDk0zgmoS2Ioe1P94ubB/kFCMf2U/RdhBdtmHQTrUW5fEQkpQkIQA7Tl7wIA70I10sptFDSZFrC96g/xL8HFENuHx4FH0Ry2hWbuOIlUUcA+hBkN0fkDbjmose1EXn4sjN8YTjn4o42X5DcTkpCm6wANJK2zWZcLj3q8hPKIpeTPkdhq4UCp0I2W5DbRNqNGvV7Bu3dExUarjni3G7Qi6pqm4yJjXQUMuHh4dbRTTT7On+zisqQcBnsXg9du5xGX/VcfHr+GwMnwyo3eg0J3+KxjYsdYq2HDX+GsljiZIvv9kVR1qNzOQJczDPSlmAueqywGL0D/TTcXJxvFEXw4SuYadFFjFfrQPPZOBPJ44TIB72LILMQT3sEENj50D8GjjWpYiXh+yYOLWD+7baJWI9PaVCYjJmsY+p2wO+OmL7lcNuFQu5E0n7zUNhOxHgZsVYaYvFN4mnY7DfYOKsQZxXeCBjzJvA48Chw0HG5e9cO5utizzOdL/w1SUjScCThsK3xsJsIGyZRIr7H82+Zf5aUgQwxGmFzKLgejOP/o/jVAjs0/0gAAAAASUVORK5CYII='
    },
    logName: {
      type: String
    },
    content: {
      type: String
    },
    contentColor: {
      type: String,
      value: '#888888'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false
  },
  /**
   * 组件的方法列表
   */
  methods: {
    cancelCallback() {
      this.triggerEvent('cancel')
    },
    hide() {
      this.setData({
        show: false
      })
    },
    show() {
      this.setData({
        show: true
      })
    },
    onGotUserInfo(e) {
      this.triggerEvent('confirm', e)
    }
  }
})
