/**
 * @Author: PerfectYan
 */

import fetch from 'fetch'

import { API_DOMAIN } from './config'


/**
 * @param {string} action 接口请求地址
 * @param {object} params [params={}]
 * @param {object} header [header={}]
 */
const fetchApi = (action, params = {}, header = {}) => {
    return fetch('POST', `${API_DOMAIN}/${action}?`, params, header)
};

/**
 * @param {string} action 接口请求地址
 * @param {object} params [params={}]
 * @param {object} header [header={}]
 */
const fetchGetApi = (action, params = {}, header = {}) => {
    return fetch('GET', `${API_DOMAIN}/${action}?`, params, header)
};

//用户注册
const Register = (params) => {
    return fetchApi('account/register', params).then(res => res)
};

//发送验证码
const SendMsgCode = (params) => {
    return fetchApi('account/captcha', params).then(res => res)
};


const Login = (params, header) => {
    return fetchApi('bh/wxapp/login', params, header).then(res => res)
};

const GetCategoryList = (params) => {
    let header = {
        bhSessionId : '38b7a8b3597847bfbc8e6354042b9064'
    };
    return fetchGetApi('bh/wxapp/category', params, header).then(res => res)
};

const GetSiteList = (params) => {
    let bhSessionId = wx.getStorageSync('bhSessionId');
    let header = {
        bhSessionId : bhSessionId
    };
    return fetchApi('bh/wxapp/init', params, header).then(res => res)
};

module.exports = {
    Register,
    Login,
    SendMsgCode,
    GetCategoryList,
    GetSiteList
};