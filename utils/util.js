/**
 * 验证码倒计时特效
 */
const getSms = {
    countdown: 60,
    setTime: function(scope) {
        clearTimeout(timer);
        let self = this;
        if (self.countdown === 0) {
            scope.setData({
                isShow: true
            });
            self.countdown = 60;
            clearTimeout(timer);
            return
        } else {
            scope.setData({
                isShow: false,
                lastTime: self.countdown
            });
            self.countdown--;
        }

        var timer = setTimeout(function() {
            getSms.setTime(scope)
        }, 1000)
    }
};


const Toast = {
    warn: function(val) {
        wx.showToast({
            title: val,
            icon: 'none',
            duration: 2000,
            mask: true
        })
    },
    loading: function() {
        wx.showToast({
            icon: 'loading',
            duration: 2000,
            mask: true
        })
    },
    success: function(val) {
        wx.showToast({
            title: val,
            icon: 'success',
            duration: 1500,
            mask: true
        })
    },
    error: function(val) {
        wx.showToast({
            title: val,
            image: '/images/others/icon-error.png',
            duration: 1500,
            mask: true
        })
    },
    alert: function(param) {
        wx.showModal({
            content: param,
            title: '提示',
            showCancel: false
        })
    },
    showLoading: function(param = 'Loading') {
        wx.showLoading({
            title: param,
            mask: true
        })
    },
    hideLoading: function() {
        setTimeout(() => {
            wx.hideLoading()
        }, 100)
    },
    showModal: function(param = 'msg') {
        wx.showModal({
            title: '',
            content: param,
            showCancel: false
        })
    }
};

module.exports = {
    getSms,
    Toast
};