
const api = require('utils/api');
import { Toast } from './utils/util';

App({
    api: api,
    Toast: Toast,
    globalData: {
        userInfo: wx.getStorageSync("userInfo") || {},
    },
    onLaunch: function () {
        console.log(this.globalData.userInfo)
    }
});
